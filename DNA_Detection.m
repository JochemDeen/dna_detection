function dnamaps = DNA_Detection(dataFolder,mlFolder,fileExpPar,varargin)
% DNA_detection(dataFolder,mlFolder,fileExpPar)
% dataFolder, folder that contains the data
% mlFolder, folder that contains the machine learned segmented data
% fileExpPar, file that contains the experimental details
%
% Optional:
% 'plotson' enable plotting
% 'verbose' enable verbosity
% 'minlengthDNA',x Set minimale length to x um
% 'dye','x' set dye to x (default is RhodamineB)
% 'labellingAgent','x' set labellingAgent to x (default is M.TaqI)
% 'Parameters',pars set parameters with (x).name and (x).value
%

%Default values
verbose = false;
minlengthDNA = 10; % in microns
savemat = true;
parameters=[];
plotson = false;
pars=[];
SIM = false;

%Default parameter
varargin{end+1}='MOMENTUM';

% initialization
%experiment parameters
dye = 'RhodamineB';
labellingAgent = 'M.TaqI';

%Experimental parameters
run(fileExpPar);

minlengthDNA = 1e3*minlengthDNA/(parameters.optics.pixel_size_nm); %Convert to pixels

p.closingLength = 40;
p.maxWidth = 8;
p.minLength = round(minlengthDNA*0.9);
p.tileSize = 2000;
p.cutoff = 8;
p.Threshold = 0.3*255; %Threshold of 0.3 * max


plt = 0;
for ii = 1:length(varargin)
    if ischar(varargin{ii})
        switch upper(varargin{ii})
            case 'PLOTSON'
                plotson = true;
                plt = 1;
            case 'VERBOSE'
                verbose = true;
            case 'MINLENGTHDNA'
                minlengthDNA = varargin{ii+1};
            case 'DYE'
                dye = varargin{ii+1};
            case 'LABELLINGAGENT'
                labellingAgent = varargin{ii+1};
            case 'SIM'
                SIM = true;
                sim_ext = varargin{ii+1};
            case 'PARAMETERS'
                scanned_params = varargin{ii+1};
                for n = 1:numel(scanned_params)
                    settemp(scanned_params(n).value);
                    eval(['pars.' scanned_params(n).name '=tempVal;']);
                    eval(['p.' scanned_params(n).name '=tempVal;']);
                end
        end
    end
end

parameters.labellingAgent = labellingAgent;


listPred = dir(mlFolder);
listPred(1:2) = [];

% Select files that match .tif and contain _preds
for k = numel(listPred):-1:1
    if isempty(strfind(listPred(k).name,'.tif')) || isempty(strfind(listPred(k).name,'_pred'))
        listPred(k) = [];
    end
end

if verbose
    for k = 1:numel(listPred)
        disp(['File # ',num2str(k),' : ',listPred(k).name])
    end 
end

% segment the list of CNN processed image
ii=0;
for f = 1:numel(listPred)
    %Get filenames
    ind = strfind(listPred(f).name,'.');
    fnameC = listPred(f).name(1:ind(end)-1);
    fnameO= listPred(f).name(1:ind(end)-6);   
    ext = listPred(f).name(ind(end):end);
    
    disp(['Fitting: ' fnameO ext]);
    
    %Load ML image
    im_ML = double(imread(fullfile(mlFolder,[fnameC ext])));
    
    %Load original image
    if ~isfile(fullfile(dataFolder,[fnameO ext]))
        warning([fullfile(dataFolder,[fnameO ext]) , ' does not exist.'])
        continue
    end
    imO = double(imread(fullfile(dataFolder,[fnameO ext]))); 
    
    %Cut part of image for cluster detection
    im=im_ML(p.cutoff:end-p.cutoff,p.cutoff:end-p.cutoff); %
    
    %Load SIM image
    if SIM
        try 
            fnameSIM = [fnameO(1:end-2) sim_ext];
            SIMimg = double(imread(fullfile(dataFolder,fnameSIM))); 
        catch
            warning('Failed to load SIM image')
            SIMimg=[];
        end
    end
            

    
    %Detect clusters
    imBin = im > p.Threshold; 
    [c,imC] = clusterIm(imBin,20);
        
    % Extraction of DNA fragments     
    lines_=extractLinesCNN(imC,c,p.closingLength,p.maxWidth,p.closingLength,plt); %2 for plotting all p.minLength,plt)
    
    if plotson
        %plot original image
        ims=sort(imO(:));
        figure(999);cla;imagesc(imO);
        caxis([ims(round(0.01*numel(ims))),ims(round(0.99*numel(ims)))])
        hold on
    end
    
    res=ii;
    %Extract traces
    for k=1:numel(lines_)
        %Extract line points, sort and add back limit
        xy = lines_(k).xy+p.cutoff;
        
        %Check if length large enough
        lengthfr = sqrt((xy(1,end)-xy(1,1))^2+(xy(2,end)-xy(2,1))^2);
        
        if lengthfr>=minlengthDNA
            ii=ii+1;
            try
                %Fit trace with snake algorithm
                [trace,xy] = FitTrace(imO,im_ML,xy,varargin{:}); 

                %Plotting
                plotxy(plotson,verbose,xy,ii,k,res,numel(trace))
                PlotTrace(plotson,imO,xy,779)
                
                %Create DNAmaps entry
                dnamaps(ii) = DNAMap();

                %Add to DNAmaps
                dataTrace = MakeDataTrace(trace,1,fullfile(dataFolder,[fnameO ext]),dye,parameters,xy,4);
                dnamaps(ii).AddDataTrace(dataTrace);
                
                if SIM && ~isempty(SIMimg)
                    %Adjust xy values for increased resolution of SIM
                    xy = distributePoints(xy.*2,1);
                    [x,y] = GeneratePerpendicular(xy);
                    PlotTrace(plotson,SIMimg,xy,778)

                    %Extract SIMTrace
                    SIMtrace  = Getinterp2(SIMimg, x,y, 'cubic');
                    SIMtrace = sum(SIMtrace ,1);
                    if plotson
                        
                    end

                    %Add to DNA maps
                    dataTrace = MakeDataTrace(SIMtrace,2,fullfile(dataFolder,[fnameSIM ext]),dye,parameters,xy,4);
                    dnamaps(ii).AddDataTrace(dataTrace);
                end

            catch
                ii = ii - 1;                
                plotxy(plotson,verbose,xy,ii,k,res,-1)
            end
        else
            plotxy(plotson,verbose,xy,ii,k,res,0)
        end
    end
    disp(['Processing : ',num2str(100*f/numel(listPred)),' [%]'])
    
    if plotson
        figure(999);
        if ~exist(fullfile(dataFolder,[datestr(now,'yyyyddmm') 'Mapped'], 'dir'))
            mkdir(fullfile(dataFolder,[datestr(now,'yyyyddmm') 'Mapped']))
        end
        saveas(gcf,fullfile(dataFolder,[datestr(now,'yyyyddmm') 'Mapped'],[fnameO 'Mapped.png']))
    end
        
end
close all

if savemat
    %Save all data
    fn=savednamaps(dataFolder,dnamaps,pars);
    Create_m_file(dataFolder,fn);
end
if nargout<1
    clear dnamaps;
end

function settemp(value)
assignin('caller', 'tempVal', value);

function plotxy(plotson,verbose,xy,ii,k,res,length)
if length>0;clr = 'black';else;clr='red';end
if plotson
    figure(999);
    x=xy(1,:);y=xy(2,:);
    %plot([xy(1,1),xy(1,end)],[xy(2,1) xy(2,end)],'LineWidth',1,'Color',clr);
    plot(x,y,'LineWidth',1,'Color',clr);
    title([num2str(ii-res) ' maps added ' num2str(k-ii+res) ' rejected'])
end
if verbose>1
    if length>0
        disp(['Add map ' num2str(ii) ' size: ' num2str(length)]);
    elseif length == 0
        lengthfr = sqrt((xy(1,end)-xy(1,1))^2+(xy(2,end)-xy(2,1))^2);
        disp(['Too short : ' num2str(lengthfr)])        
    else
        disp('Error in fitting!')
    end
end

function dataTrace = MakeDataTrace(trace,SIM,file,dye,parameters,xy,width)
parameters.optics.pixel_size_nm = parameters.optics.pixel_size_nm/SIM;
dataTrace = SIMDataTrace(trace, SIM,file); %1 for WF, 2 for SIM 
dataTrace.SetDye(dye); 
dataTrace.SetParameters(parameters);
dataTrace.SetROI(xy(1,:), xy(2,:), width); %optional 

function PlotTrace(plotson,img_,xy,fig)
if plotson
    x=xy(1,:);y=xy(2,:);
    figure(fig);cla
    imagesc(img_.*-1); hold on;
    colormap gray
    axis equal;
    xlim([min(x)*0.9,max(x)*1.1]);
    ylim([min(y)*0.9,max(y)*1.1]);
    plot(x,y,'--','LineWidth',2,'Color','Red');
    set(gca,'YTick',[])
    set(gca,'XTick',[])
    s_img=sort(img_(:)).*-1;
    caxis([s_img(round(0.99*numel(s_img))),s_img(round(0.05*numel(s_img)))])
    pause(0.01);
end