function [trace, xy] = FitTrace(img,MLimg,xy,varargin)
%xy in format [x1, x2;y1, y2]

pars.stepsize = 1;
for ii = 1:length(varargin)
    if ischar(varargin{ii})
        switch upper(varargin{ii})
            case 'PARAMETERS'
                scanned_params=varargin{ii+1};
                for n = 1:numel(scanned_params)
                    settemp(scanned_params(n).value);
                    eval(['pars.' scanned_params(n).name '=tempVal;']);
                end
        end
    end
end

xy = distributePoints(xy,pars.stepsize);

%Fit with snake algorithm
xy = SnakeFit(img,MLimg,xy,varargin{:});

%Extract Perpendicular datapoints
[x,y] = GeneratePerpendicular(xy);
trace = Getinterp2(img, x,y, 'cubic');
trace = sum(trace,1);
if nargout<2
    clear xy
end

function settemp(value)
assignin('caller', 'tempVal', value);
