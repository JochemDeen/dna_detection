folder2 = 'I:\Shared drives\ADgut\Mixed Sample_Laurens\20191121_CNN_pred';
folder1 = 'I:\Shared drives\ADgut\Mixed Sample_Laurens';

fileExpPar = 'Experimental_Parameters_Leuven_SIMWF.m';

pars(1).name = 'k2';
pars(1).value = 0;
pars(2).name = 'a';
pars(2).value = 0;
pars(3).name = 'maxWidth';
pars(3).value = 7;

DNA_Detection(folder1, folder2, fileExpPar, 'Parameters',pars,'verbose','plotson','SIM','adrienSIM.tif');