
path = uigetdir(cd,'Select folder with CNN processed data');

listPred = dir(path);
listPred(1:2) = [];

for k = numel(listPred):-1:1
    if isempty(strfind(listPred(k).name,'.tif'))
        listPred(k) = [];
    elseif isempty(strfind(listPred(k).name,'_pred'))
        listPred(k) = [];
    end
end

for k = 1:numel(listPred)
    disp(['File # ',num2str(k),' : ',listPred(k).name])
end 

%% segment the list of CNN processed image
CNNRois = [];
for f = 1:1%numel(listPred)
    ind = strfind(listPred(f).name,'.');
    fnameC = listPred(f).name(1:ind(end)-1);
    ext = listPred(f).name(ind(end):end);
    im = double(imread([path,filesep,fnameC,ext]));
    
    imBin = im > 0.3*255;
    [c,imC] = clusterIm(imBin,20);
    
    %% Estimate average angle
%     disp('Estimation of DNA orientation')
    tileSize=2000;
    o=estimOrien(imC,tileSize,1);
    
    %% Extraction of DNA fragments 
    
%     disp('Extraction of DNA traces')
    closingLength= 20;
    maxWidth=7;
    oTh=3;
    minLength = 20;
    
    line=extractLinesCNN(imC,c,closingLength,maxWidth,minLength,o,oTh,1);
%     extractLines(im,imC,c,o,closingLength,maxWidth,minLength,orientationThreshold,5)
    
    %% save ROI 
    for k=1:size(line,1)       
        CNNRois{f}(k).x1=line(k,1);   CNNRois{f}(k).y1=line(k,2);
        CNNRois{f}(k).x2=line(k,3);   CNNRois{f}(k).y2=line(k,4);
        CNNRois{f}(k).name = fnameC;
    end
    disp(['Processing : ',num2str(100*f/numel(listPred)),' [%]'])
end

%% make a new folder with CNN ROIs, ready to be quantified against the final_seg
ind = strfind(path,filesep);
rootDir = path(1:ind(end));
foldName = path(ind(end)+1:end);
foldName = strrep(foldName,'pred','rois');
mkdir([rootDir,foldName])

for f = 1:numel(CNNRois)
    try
        disp(CNNRois{f}(1).name)
        WriteImageJROIs(CNNRois{f},[path,filesep,CNNRois{f}(1).name,'_ROI.zip']);
        % writeTIFF(im,[rootDir,foldName,filesep,rois{f}(k).name],[0.108 0.108])
    end
end