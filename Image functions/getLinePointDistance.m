function d = getLinePointDistance(p1,p2,P)

d = abs((p2(2)-p1(2))*P(1) - (p2(1)-p1(1))*P(2) + p2(1)*p1(2) - p2(2)*p1(1))/sqrt(sum((p2-p1).^2));