% c : list of clusters
% im : original image
function out = getClusterParam(c,ref,disp)

if nargin < 3; disp = 0 ; end

% create binary image from cluster
im = [];
refIm = ref(min(c(:,2)):max(c(:,2)),min(c(:,1)):max(c(:,1)));
offy = min(c(:,2)); offx = min(c(:,1));
c(:,2) = c(:,2) - offy+1; c(:,1) = c(:,1) - offx+1; 
for k = 1:size(c,1)
    im(c(k,2),c(k,1)) = 1;
end
padsize = max(ceil(1.5*size(refIm)/4));
refIm = padarray(refIm,[padsize padsize],0);
im = padarray(im,[padsize padsize],0);

%% find cluster main axis and orientation
w = (refIm.*im);
an = linspace(-90,90,25);an(end) = [];
t = [];
for k = 1:length(an)
    t(k) = max(sum(imrotate(w,an(k),'bilinear','crop'),2));
end
[~,ind] = max(t);
% refine the angle giving max projection
if ind == 1 ; ind = 2; end
if ind == 24; ind = 23; end
an2 = linspace(an(ind-1),an(ind+1),25);
t = [];
for k = 1:length(an2)
    t(k) = max(sum(imrotate(w,an2(k),'bilinear','crop'),2));
end
[~,ind] = max(t);
or = an2(ind); % best orientation
imRot = imrotate(w,or,'bilinear');
prof = sum(imRot,2);
[~,off] = max(prof); % measure offset of rotated profile

% compute rotation angle in degrees
out.or = or;
imRot = imrotate(im,out.or,'bilinear');
imRot2 = imrotate(w,out.or,'bilinear');

out.sx = sum(max(imRot>0.1,[],1)); % large axis length
out.msy = max(sum(imRot2>0.1,1)); % maximum transversal FWHM of transversal length
out.sym = max(sum(imRot,1)); %max of transversal length

centerRot = [(size(imRot,2)/2); (size(imRot,1)/2)];
center = [(size(refIm,2)/2); (size(refIm,1)/2)];

M = @(or) [cosd(or) sind(or); -sind(or) cosd(or)];

prof = sum(imRot2>0.1,1); ind = find(prof >= 1);
v = imRot2(:,ind);
y = sum(v.*repmat(((1:size(v,1))'),[1 size(v,2)]),1)./sum(v,1);
y=(imresize(y,0.3,'bilinear')); ind=(imresize(ind,0.3,'bilinear'));
Pp = [ind - centerRot(1); y-centerRot(2)];
Ap = Pp(:,1); Bp = Pp(:,end);
P = M(-or)*Pp + center;

A = P(:,1);
B = P(:,end);

% v1 = imRot2(:,ind(1)); v2 = imRot2(:,ind(end));
% y1 = sum(v1.*((1:length(v1))'))/sum(v1);
% y2 = sum(v2.*((1:length(v2))'))/sum(v2);
% Ap = [min(ind) - centerRot(1); y1-centerRot(2)]; Bp = [max(ind)-centerRot(1); y2-centerRot(2)];
% A = M(-or)*Ap+center; B = M(-or)*Bp+center;
% n=3;
% linspacex = linspace(min(ind),max(ind),n+2);
% C=[];
% for ii=2:n+1
%     x = linspacex(ii);
%     v_ = imRot2(:,round(x));
%     y_ = sum(v_.*((1:length(v_))'))/sum(v_);
%     Cp = [x - centerRot(1); y_ - centerRot(2)];
%     C = [C ; M(-or)*Cp + center];
% end

out.W = size(imRot,2);
out.H = size(imRot,1);
out.m = (B(2)-A(2))/(B(1)-A(1));
out.b =  B(2) - out.m*B(1);
out.A = A - padsize + [offx;offy]; out.B = B - padsize + [offx;offy];
% out.C = C - padsize + repmat([offx;offy],n,1);
out.P = P- padsize + [offx;offy];
sy = sum(imRot2>0.1,1);
out.sy = sy(round(ind)); % transversal length

if disp
    figure(disp)
    subplot(221)
        imagesc(log(refIm+1)); hold on
        plot([A(1) B(1)],[A(2) out.b+out.m*B(1)],'r','linewidth',2)
        plot([A(1) B(1)],[A(2) B(2)],'gx','linewidth',2)
        for k = 2:10:size(P,2)-1
            plot(P(1,k),P(2,k),'gx','linewidth',2)         
        end
        hold off
        title(['Center x : ',num2str(center(1)),', center y : ',num2str(center(2))])
    subplot(222)
        imagesc(imRot)
        title(['Orientation : ' num2str(out.or),', sx : ',num2str(out.sx),', sy : ',num2str(max(out.sy))])
        hold on
        plot([Ap(1) Bp(1)]+centerRot(1),[Ap(2) Bp(2)]+centerRot(2),'rx','linewidth',2)
        hold off
    subplot(223)
        imagesc(log(refIm+1))
end

end

%% mean square fitting for line detection not working - too sensitive to noise : 
% w = w(:);
% w(w == 0) = [];
% pk = pkfnd(refIm.*im,estimBcgrHistSym(w)+15,5);
% % mean square fitting
% c= pk; w = refIm(pk);%ones(size(pk,1),1);
% cx = sum(w.*c(:,1))/sum(w); cy = sum(w.*c(:,2))/sum(w);
% m = sum(w.*(c(:,1)-cx).*(c(:,2)-cy))/sum(w.*(c(:,1)-cx).*(c(:,1)-cx));
% b = cy - m.*cx;