
% DNA images is already connected and cleaned by the CNN
% this allows to skip the problem of connection, cluster orientation,
% etc...
% The only criterion is now the minLength and maxWidth  

function clines = extractLinesCNN(im,c,closingLength,maxWidth,minLength,displayT)

 if ~exist('displayT','var'); displayT = 0; end
% for each clusters c
lines = {}; count = 1;
for k = 1:numel(c)
    tempC = c{k};
    param=getClusterParam(c{k},im,(displayT-1)*((displayT-1)>0));
    map = param.sy < maxWidth;
    seg = zeros(1,length(map)); c2 = 1;
    for m = 1:length(map)
        if map(m)
            seg(c2,m) = m;
        else
            c2= c2 +1;
        end
    end
    [~,ind]= max(sum(seg>0,2));
    param.P = param.P(:,seg(ind,:)>0);
    if ~isempty(param.P) %&& abs(param.or-o) < oTh
        lines{count} = param.P-1;
        count = count + 1;
    end
end

%% connect the lines
temp = lines;
clines_ = []; % connected lines
while ~isempty(temp)
    merged = 0;
    p1 = temp{1}(:,1); p2 = temp{1}(:,end);
    for k = 2:numel(temp)
        pa = temp{k}(:,1); pb = temp{k}(:,end);
        % compute distance between the first line and pa and pb
        da = getLinePointDistance(p1,p2,pa);
        db = getLinePointDistance(p1,p2,pb);
        if da < 1.5 && db < 1.5
            d1a = sum((p1-pa).^2); d2a = sum((p2-pa).^2);
            d1b = sum((p1-pb).^2);d2b = sum((p2-pb).^2);
            dmin = min([d1a d2a d1b d2b]);
            [~,ind] = max([d1a d2a d1b d2b]);
            p0 = [p1 p2 p1 p2];
            if dmin < closingLength*closingLength
                tmp = [temp{1} temp{k}];
                dmap = sqrt((tmp(1,:)-p0(1,ind)).^2 + (tmp(2,:)-p0(2,ind)).^2);
                [~,map] = sort(dmap,'ascend');
                temp{k} = tmp(:,map);
                merged = 1;
                break
            end
        end
    end
    if merged == 0
        clines_{end+1} = temp{1};
    end
    temp(1) = [];        
end

%% remove too small lines
for k = numel(clines_):-1:1
    
	d = sqrt((clines_{k}(1,1)-clines_{k}(1,end))^2 + (clines_{k}(2,1)-clines_{k}(2,end))^2);
	if d < minLength
        clines_(k) = [];
	end
end

%sort lines
clines = [];
for ii = 1:numel(clines_)
    clines(ii).xy = clines_{ii};
    clines(ii).line = clines_{ii};
end

%% display
if displayT > 0
    figure(displayT)
    imagesc(im>=1); colormap(imresize([0 0 0; 0 1 0],[64,3],'bilinear')); hold on
    allplots=[];
    labels={};
    for k = 1:numel(lines)
        p1=plot([lines{k}(1,1) lines{k}(1,end)],[lines{k}(2,1) lines{k}(2,end)],'w','linewidth',5);%,'DisplayName','Detected')
        allplots(1)=p1;
        labels{1} = 'Detected';
    end
    o = 2;
    for k = 1:numel(clines)
        p2=plot(clines(k).xy(1,:), clines(k).xy(2,:),'m','linewidth',2.5);%,'DisplayName','Approved')
        allplots(2)=p2;
        labels{2}='Approved';
    end
    hold off
    lgnd = legend(allplots,labels,'Orientation','horizontal','Location','southoutside');
    set(lgnd,'color','none');
    set(lgnd, 'Box', 'off');
end