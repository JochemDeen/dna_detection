function A = MakeA(pars,lengthSeg)
a = pars.a;
b = pars.b;
g = pars.g;

cofM = [b;-(a+4*b);2*(a+3*b)+g;-(a+4*b);b];
cofM=cofM(:,ones(1,lengthSeg));

cofN = [b,-(a+4*b),2*(a+3*b)+g,-(a+4*b),b];

cofM(:,1) = [b; -2*b; a+2*b+g;-a-2*b;b];
cofM(:,2) = [0; -a-2*b; 2*a+5*b+g; -a - 4*b;b];

cofM(:,lengthSeg-1) = [b;-a-4*b; 2*a+5*b+g;-a-2*b;0];
cofM(:,lengthSeg) = [b;-a-2*b; a+2*b+g;-2*b;b];

A=zeros(lengthSeg);
for i=1:lengthSeg
    for k = 1:numel(cofN)
        cof = cofM(:,i);
        n = i - 3 + k;
        if n<1
            n = 2-n;
        end
        if n>lengthSeg
            n = lengthSeg - k + 3;
        end
        
        A(n,i)= A(n,i) + cof(k);
    end
end