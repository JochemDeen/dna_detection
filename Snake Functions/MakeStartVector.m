function newxy = MakeStartVector(xy, stepPx)
%xy=[x1,x2;y1,y2];

if nargin<2
    stepPx=1;
end

diffXY = diff(xy, 1, 2).^2;

lengthTrc = sqrt(sum(diffXY));

nrPx = round(lengthTrc)/stepPx;

tail = (lengthTrc-round(lengthTrc))/2;

Vec = linspace(tail, round(lengthTrc)-tail, nrPx+1);

normVec = Vec/round(lengthTrc);

newxy = xy(:,ones(1,nrPx+1))+diffXY.*[normVec;normVec];