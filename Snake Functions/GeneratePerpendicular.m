function [x,y] = GeneratePerpendicular(xy,width)
if nargin<2
    width = 4;
end
x_ = xy(1,:);
y_ = xy(2,:);


Vector = xy(:,1:end-1)-xy(:,2:end);

Pvector =[-Vector(2,:)./Vector(1,:);ones(1,length(Vector))];
Pvector = Pvector ./sqrt(sum(Pvector.^2));
Pvector(:,end+1) = Pvector(:,end);
Pvector_x= Pvector(1,:);
Pvector_y= Pvector(2,:);

C = [-width:width]';
PerM = C(:,ones(length(xy),1));


x = x_ +PerM.*Pvector_x;
y = y_ +PerM.*Pvector_y;


