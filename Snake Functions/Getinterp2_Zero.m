function intensity = Getinterp2_Zero(img,x,y,varargin)

img_z = ones(size(img)+2).*-1;
img_z(2:end-1,2:end-1) = img;
x=x+1;
y=y+1;

intensity = Getinterp2(img_z,x,y,varargin{:});