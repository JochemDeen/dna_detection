function xy = SnakeFit(img,img2,xy,varargin)
% img Image to be fitted
% img2 Mask Image
% xy starting points


%deal with varargins
plotson = false;
gaussFilter= false;
momentum = false;

pltsteps = 25;
pars.a = 60; %1e-3; %limit size
pars.b = 2000; %Bending energy
pars.g = 50; %Step size gradient descent
pars.k1 = 10; % External energy of image
pars.k2 = 20; %Stretching Energy of snake
pars.stepsize = 1;
pars.niter = 1000;
pars.intV = 1;
pars.threshold = 1e-5;
pars.mask = true;

for ii = 1:length(varargin)
    if ischar(varargin{ii})
        switch upper(varargin{ii})
            case 'PLOTSON'
                plotson = true;
            case 'PLTSTEPS'
                pltsteps = varargin{ii+1};
            case 'PARS'
                pars = varargin{ii+1};
            case 'GAUSSFILTER'
                gaussFilter = true;
            case 'MOMENTUM'
                momentum = true;
            case 'PARAMETERS'
                scanned_params=varargin{ii+1};
                for n = 1:numel(scanned_params)
                    settemp(scanned_params(n).value);
                    eval(['pars.' scanned_params(n).name '=tempVal;']);
                end
        end
    end
end


%Initial values
img_ = img;
x0=xy(1,:);
y0=xy(2,:);

%Mask with ML image
if pars.mask
    img = img.*img2;
end

%Oriented Gaussian Filter
if gaussFilter
    [X,Y] = meshgrid(1:10,1:10);
    sigmax=4;sigmay=1;yc=5.5;xc=5.5;
    gauss=exp(-(X-xc).^2/(2*sigmax^2)-(Y-yc).^2/(2*sigmay^2));
    tileSize=2000;
    o=estimOrien(img2,tileSize,1);
    img = conv2(img,imrotate(gauss,-o),'same');
end

% img = imgaussfilt(img,3);
img = img - median(img);
img = img/std(double(img(:)));
if plotson
    figure(789);cla
    axis equal;
    imagesc(img); hold on;
    xlim([min(x0)*0.9,max(x0)*1.1]);
    ylim([min(y0)*0.9,max(y0)*1.1]);
end

[gx, gy] = gradient(img);

oldxy=xy;diff=nan;
oldSize = size(oldxy,2);
g_text='';

for ii = 0:pars.niter
    
    if size(xy,2)>1500
        error('Size too long')
    end
    
    if plotson && mod(ii,pltsteps)==0
        if momentum
            g_text=[' - gamma: ' num2str(round(pars.g)) ' '];
        end
        figure(789);
        plot(xy(1,:),xy(2,:),'--','LineWidth',2); title(['Round ' num2str(ii) ' - change: ' num2str(diff) g_text])
        pause(0.01);
    end
    
    %Compute Pentadiagonal matrix for inner energy
    A = MakeA(pars,size(xy,2));
    
    %Image energy
    fx = Getinterp2_Zero(gx,xy(1,:),xy(2,:),'cubic');
    fy = Getinterp2_Zero(gy,xy(1,:),xy(2,:),'cubic');
    vf = pars.k1 *[fx;fy]/pars.intV;
    
    % Normalized vectors of fiber ends
    v_start = xy(:,1) - xy(:,2);
    v_start = v_start/sqrt(sum(v_start.^2));
    v_end = xy(:,end) - xy(:,end-1);
    v_end = v_end/sqrt(sum(v_end.^2));
    
    %Image intensities at the ends
    int_start = double(Getinterp2_Zero(img, xy(1,1), xy(2,1), 'cubic'));
    int_end = double(Getinterp2_Zero(img, xy(1,end), xy(2,end), 'cubic'));
    
    %Add stretching energy
    vf(:,1) = vf(:,1) + pars.k2*v_start.*int_start/pars.intV;
    vf(:,end) = vf(:,end) + pars.k2*v_end*int_end/pars.intV;
    
    %Compute new values
    newXY = (pars.g*xy+vf)/A;
    
    %Distribute points
    xy = distributePoints(newXY,pars.stepsize);
    
    %if size(oldxy)~=size(xy) % no change in size
    beginfr = 1;
    if sum(oldxy(:,1)-xy(:,1))>sum(oldxy(:,2)-xy(:,2))
        beginfr =2 ;
    end
    endfr = min(size(oldxy,2),size(xy,2));
    
    if ii>=2;diff0=diff;end
    diff=sum(sum((oldxy(:,beginfr:endfr)-xy(:,beginfr:endfr)).^2,2),1)/(endfr-beginfr+1);
    szdiff = oldSize - size(xy,2);
    oldSize = size(oldxy,2);
    
    if diff<pars.threshold && abs(szdiff)<1
        break
    end
    
    if ii>=2 && momentum
        diff2=diff0-diff;
        att = (diff2>=0)*0.99+(diff2<0)*1.01-ii/10000; %+(diff2<0)*min(1.01-ii/1e4,max(0.9,diff/1e-3));
        pars.g = pars.g/att;
        pars.g = min(max(20,pars.g),2000); %limit to 20 and 1000
        %pars.g=pars.g/((1.01-ii/1e4)*min(1,max(0.9,diff/1e-3)));
    end
    oldxy=xy;
end

function settemp(value)
assignin('caller', 'tempVal', value);
