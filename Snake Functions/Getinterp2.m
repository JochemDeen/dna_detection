function intensity = Getinterp2(img,x,y,varargin)

x(x<1) = 1;
x(x>size(img,2)) = size(img,2);
y(y<1) = 1;
y(y>size(img,1)) = size(img,1);

intensity = double(interp2(img,x,y,varargin{:}));